import React from "react"
import {graphql} from "gatsby"
import BasicProjectPage from "../../components/BasicProjectPage"
import video from "../../video/NameProject.mp4"

const NameAnimation = ({data})=>{
    const projectData = data.allProjectdataJson.edges[0].node

    return (
        <BasicProjectPage projectData={projectData}>
            <video muted autoPlay loop width="960" height="640" style={{
                margin:'0.8vw',
                width:'90%',
                height:'90%'
            }}>
                <source src={video} type="video/mp4"/>
            </video>
        </BasicProjectPage>
    )
}
export default NameAnimation

export const query = graphql`
    query Query {
        allProjectdataJson(filter : {title: {eq: "Name Animation"}}){
            edges{
                node{
                    title
                    description
                    made_with
                    links{
                        link
                        type
                    }
                    images{
                        childImageSharp{
                            gatsbyImageData
                        }
                    }
                }
            }
        }
    }    
`