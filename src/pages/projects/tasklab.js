import * as React from "react"
import { graphql } from "gatsby"

import ProjectPage from "../../components/ProjectPage"


const TaskLab = ({data}) => {
    const projectData = data.allProjectdataJson.edges[0].node

    return(
        <ProjectPage projectData={projectData}/>
    )
}

export const query = graphql`
    query TaskLabQuery {
        allProjectdataJson(filter: {title: {eq: "TaskLab"}}) {
            edges {
                node {
                    title
                    description
                    made_with
                    links {
                        link
                        type
                    }
                    images {
                        childImageSharp {
                        gatsbyImageData
                        }
                    }
                }
            }
        }
    }
`


export default TaskLab
