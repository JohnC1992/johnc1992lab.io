import React from "react"
import { graphql } from "gatsby"
import BasicProjectPage from "../../components/BasicProjectPage"

const CitadelClash = ({data}) =>{
    const projectData = data.allProjectdataJson.edges[0].node

    return(
        <BasicProjectPage projectData={projectData}>
            <iframe frameborder="0"
            src="https://itch.io/embed-upload/9353833?color=663399"
            allowFullScreen={true} width="960" height="660"
            style={{
                marginLeft:'0.8vw',
                marginTop:'0.8vw'
            }}>
                <a href="https://karaokeczar.itch.io/tower-defense">
                    Play Citadel Clash on itch.io
                </a>
            </iframe>
        </BasicProjectPage>
    )
}
export default CitadelClash

export const query = graphql`
    query Query {
        allProjectdataJson(filter: {title: {eq: "Citadel Clash"}}) {
            edges {
                node {
                    title
                    description
                    made_with
                    links {
                        link
                        type
                    }
                    images {
                        childImageSharp {
                        gatsbyImageData
                        }
                    }
                }
            }
        }
    }
`
