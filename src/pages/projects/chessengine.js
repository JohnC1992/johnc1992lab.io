import React from "react"
import {graphql} from "gatsby"
import BasicProjectPage from "../../components/BasicProjectPage"

const ChessEngine = ({data})=>{
    const projectData = data.allProjectdataJson.edges[0].node

    return (
        <BasicProjectPage projectData={projectData}>
           <iframe width="560" height="315" 
           src="https://www.youtube.com/embed/Ne40a5LkK6A?si=kFOpu8UMLzd0YK0e" 
           title="YouTube video player" frameBorder="0" 
           allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
           referrerPolicy="strict-origin-when-cross-origin" 
           allowFullScreen
           style={{
            margin:"0.8vw",
            width:"98%"
           }}
           >
           </iframe>
        </BasicProjectPage>
    )
}
export default ChessEngine

export const query = graphql`
    query Query {
        allProjectdataJson(filter : {title: {eq: "Chess Engine"}}){
            edges{
                node{
                    title
                    description
                    made_with
                    links{
                        link
                        type
                    }
                    images{
                        childImageSharp{
                            gatsbyImageData
                        }
                    }
                }
            }
        }
    }    
`