import * as React from "react"

import Layout from "../../components/layout"
import Seo from "../../components/seo"
import ProjectCard from "../../components/ProjectCard"
import {projectList, portfolioSection, divider} from '../../styles/home.module.css'

const ProjectsPage = () => {
    return(
    <Layout>
        <Seo title="Projects" />
        <div className={portfolioSection}>
            <h1>My work</h1>
            <div className={divider}/>
            <div className={projectList}>
                <ProjectCard dataIndex={0}/>
                <ProjectCard dataIndex={1}/>
                <ProjectCard dataIndex={2}/>

            </div>
        </div>
    </Layout>
    )
}

export default ProjectsPage
