import * as React from "react"
import { graphql } from "gatsby"

import ProjectPage from "../../components/ProjectPage"


const Randime = ({data}) => {
    const projectData = data.allProjectdataJson.edges[0].node

    return(
        <ProjectPage projectData={projectData}/>
    )
}

export const query = graphql`
    query randimeQuery {
        allProjectdataJson(filter: {title: {eq: "Randime"}}) {
            edges {
                node {
                    title
                    description
                    made_with
                    links {
                        link
                        type
                    }
                    images {
                        childImageSharp {
                        gatsbyImageData
                        }
                    }
                }
            }
        }
    }
`


export default Randime
