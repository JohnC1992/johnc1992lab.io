import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import ProjectCard from "../components/ProjectCard"
import GitlabIcon from "../components/icons/GitlabIcon"
import LinkedinIcon from "../components/icons/LinkedinIcon"
import {projectList, portfolioSection, divider, bioSection, bio, portrait, links} from '../styles/home.module.css'


const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <div className={bioSection}>
      <div className={bio}>
        <h1>John Coleman</h1>
        <div>Web Developer • Programmer <br></br> Game Designer</div>
        <div className={links}>
          <span/><span/><span/>
          <GitlabIcon link='https://gitlab.com/JohnC1992'/>
          <LinkedinIcon link='https://www.linkedin.com/in/colemanja/'/>
        </div>
      </div>
    </div>
    <div className={portfolioSection}>
      <h1 style={{textAlign:"left"}}>Projects</h1>
      <div className={divider} style={{marginLeft:"0"}}/>
      <div className={projectList}>
          <ProjectCard dataIndex={4}/>
          <ProjectCard dataIndex={5}/>
          <ProjectCard dataIndex={3}/>
          <ProjectCard dataIndex={0}/>
          <ProjectCard dataIndex={1}/>
          <ProjectCard dataIndex={2}/>

      </div>
    </div>
  </Layout>
)

export default IndexPage
