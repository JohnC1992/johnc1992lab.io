import * as React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"


const Header = () => (
  <header
    style={{
      background: `rebeccapurple`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <Link
        style={{
          color: 'white',
          textDecoration: 'none',
          fontSize: '1.5rem',
        }}
        to="/"
      >
          Home
      </Link>

    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
