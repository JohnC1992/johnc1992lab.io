import React from "react"
import Layout from "./layout"
import Seo from "./seo"
import ProjectPageTable from "./ProjectPageTable"
import {
    sideMargin,
    divider,
    grid2,
    roundedPurple,
    shadow,
} from '../styles/projectpage.module.css'

const BasicProjectPage = ({projectData,children,style})=>{
    return(
        <Layout>
            <Seo title={projectData.title}/>
            <div className={sideMargin}style={style}>
                <h1>{projectData.title}</h1>
                <div  className={divider}/>
                <div>
                    <div className={`${grid2} ${roundedPurple} ${shadow}`}>
                        {children}
                        <ProjectPageTable projectData={projectData}
                        style={{
                            margin:'1vw',
                            border:0,
                            height:"100%"
                        }}
                        tableStyle={{
                            height:"80%"
                        }}
                        />
                    </div>
                    <div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default BasicProjectPage
