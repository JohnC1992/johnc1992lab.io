import React from "react"
import GitlabIcon from "./icons/GitlabIcon"
import {infoTable, purpleBorder} from '../styles/projectpage.module.css'
import ItchIcon from "./icons/ItchIcon"

export default function ProjectPageTable({projectData,children, style, tableStyle}){
    return(
        <div className={purpleBorder} style={style}>
            <table className={`${infoTable}`} style={tableStyle}>
                <tbody>
                    <tr>
                        <td>
                            <strong>Made With</strong>
                        </td>
                        <td>
                            {projectData.made_with.join(' | ')}
                        </td>
                    </tr>
                    <tr>
                        <td colSpan={2}>
                        {projectData.description}
                        </td>
                    </tr>
                    <tr>
                        <td colSpan={2}>
                        {projectData.links.map((linkData,i)=>{
                            if (linkData.type==='gitlab'){
                                return <GitlabIcon key={i} link={linkData.link}/>
                            }
                            if (linkData.type === 'itch'){
                                return <ItchIcon key={i} link={linkData.link}/>
                            }
                            return<span/>
                        })}
                        </td>
                    </tr>
                    <tr></tr>
                </tbody>
            </table>
            {children}
        </div>
    )
}
