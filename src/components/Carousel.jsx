import React, { useEffect, useState } from "react"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import {image,
        overlay,
        carousel,
        carouselFull,
        tracker,
        selected,
        imageFull,
        overlayFull
    } from "../styles/carousel.module.css"

export default function Carousel({props, images, handleClick, imageIndex = 0,fullSize = false}) {
    let skipClickAction = false
    const [index, setIndex] = useState(imageIndex)
    const [currentImage, setCurrentImage] = useState(getImage(images[0]))

    const handleRight = ()=>{
        skipClickAction = true
        setIndex((i)=>(i+1)%images.length)
    }
    const handleLeft = ()=>{
        skipClickAction = true
        setIndex((i)=>i===0? images.length-1 : i-1)
    }

    const _setIndex = (i) => {
        skipClickAction = true
        setIndex(i)
    }

    const clickAction = (index)=>{
        if (skipClickAction){
            skipClickAction = false
            return
        }
        handleClick?.(index)
    }

    useEffect(()=>{
        setIndex(imageIndex)
    },[imageIndex])

    useEffect(()=>{
        setCurrentImage(getImage(images[index]))
    },[index, images, setCurrentImage])

    return(
        <div className={fullSize? carouselFull:carousel}
        onClick={()=>clickAction(index)}>
            <GatsbyImage
                image={currentImage}
                alt={`example image ${index}`}
                className={fullSize? imageFull:image}
            />
            <span className={fullSize? overlayFull:overlay}>
                <button onClick={handleLeft}>⭠</button>
                <div className={tracker}>
                    {images.map((img,i)=>(
                        <span className={i===index? selected: ''}
                        onClick={()=>_setIndex(i)}
                        key={i}
                        >•</span>
                    ))}
                </div>
                <button onClick={handleRight}>⭢</button>
            </span>
        </div>
    )
}
