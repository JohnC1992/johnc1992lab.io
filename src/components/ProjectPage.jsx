import React, { useState, useRef, useEffect } from "react"
import Layout from "./layout"
import Seo from "./seo"
import Carousel from "./Carousel"
import Modal from "./Modal"
import ProjectPageTable from "./ProjectPageTable"
import {grid, sideMargin, divider} from '../styles/projectpage.module.css'

const ProjectPage = ({projectData})=>{
    const modal = useRef(null)
    const [modalIndex, setModalIndex] = useState(0)

    const showModal = ()=>{
        if(modal !== null){
            if(modal.current !== null){
                modal.current.close()
                modal.current.showModal()
            }
        }
    }
    useEffect(()=>{
        if(document !== undefined && document !== null){
            document.addEventListener("showModal",showModal)
        }
    },[])

    const handleCarouselClick = (imageIndex)=>{
        document.dispatchEvent(new CustomEvent("showModal"))
        setModalIndex(imageIndex)
    }

    return(
        <Layout>
            <Seo title={projectData.title}/>
            <Modal reference={modal}>
                <Carousel images={projectData.images} fullSize={true} imageIndex={modalIndex}/>
            </Modal>
            <div className={sideMargin}>
                <h1>{projectData.title}</h1>
                <div className={grid}>
                    <div>
                        <div  className={divider}/>
                        <ProjectPageTable projectData={projectData}/>
                    </div>
                    <Carousel images={projectData.images} handleClick={handleCarouselClick}/>
                </div>
            </div>
        </Layout>
    )
}

export default ProjectPage
