import React from "react"
import {modal} from "../styles/modal.module.css"

export default function Modal({props,children,reference}){

    const handleHide = ()=>{
        document.querySelector('#modal').close()
    }

    return(
        <dialog id="modal" className={modal} style={{}} ref={reference}>
            <button onClick={handleHide}>Close</button>
            <div>{children}</div>
        </dialog>
    )
}
