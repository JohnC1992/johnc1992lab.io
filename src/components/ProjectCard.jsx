import React from 'react'
import {Link} from 'gatsby'
import { useStaticQuery, graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import {projectCard, divider} from '../styles/card.module.css'


export default function ProjectCard({props, dataIndex}) {

  const query = useStaticQuery(graphql`
    query MyQuery {
      allProjectdataJson{
        edges{
          node{
            title
            previewImage{
              childImageSharp {
                gatsbyImageData(
                  aspectRatio: 1.5
                  width: 450
                  transformOptions: {
                    fit: COVER
                  }
                )
              }
            }
            made_with
            description
          }
        }
      }
    }
  `)

  const data = query.allProjectdataJson.edges[dataIndex].node
  const image = getImage(data.previewImage)
  const link = `/projects/${data.title.toLowerCase().split(' ').join('')}`
  const maxLength = 120
  const description = data.description.length < maxLength ?
    data.description :
    `${data.description.slice(0,maxLength-3)}...`

  return (
    <Link to={link} className={projectCard}>
      <GatsbyImage
        image={image}
        alt={data.title}
      />
      <div>
        <h2>{data.title}</h2>
        <span>{data.made_with.slice(0,5).join(' | ')}</span>
        <div className={divider}/>
        <p>{description}</p>
      </div>
      <svg viewBox='-20 -20 100 200'>
        <path
          fill='white'
          d='
          M30,25 l15,0
          l15,50
          l-15,50 l-15,0
          l15,-50 z
          '
        />
      </svg>
    </Link>
  )
}
